Backbone.sync = function(method, model, options) {
  console.log(arguments);
};

var App = {};

App.models = {};
App.collections = {};
App.views = {};

App.models.Note = Backbone.Model.extend({
	defaults: {
		title: '',
		category: 'note',
		content: '',
		important: false
	},
});

App.collections.Notes = Backbone.Collection.extend({
	model: App.models.Note,
	url: 'http://note.api/note'
});

App.views.NoteView = Backbone.View.extend({
	el: '#notes',
	template: _.template('<li class="topcoat-list__item"><%= content %></li>'),
	events: {
		'click #add-note': 'addNote'
	},
	addNote: function(e) {
		e.preventDefault();
		console.log("lalala");
		var	content = $('#content').val();
		var category = $( "input:radio[name=category]:checked" ).val(); 
		var note = new App.models.Note();
		note.set('content', content);
		note.set('category', category);
		this.collection.create(note);
		this.collection.add(note);
	},
	initialize: function() {
		this.listenTo(this.collection, 'add', this.renderNewBook);
	},
	render: function() {
		var self = this,
			content = '';
		this.collection.each(function(note) {
			content += self.template(note.toJSON());
		});
		this.$el.find('ul.topcoat-list__container').html(content);
		return this;
	},
	renderNewNote: function(note) {
    	this.$el.find('ul.topcoat-list__container').append(this.template(note.toJSON()));
  	}

});

$(document).on('ready', function() {
	var notes = new App.collections.Notes();
	notes = notes.fetch();
});
