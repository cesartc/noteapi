<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	
	<script type="text/javascript" src={{URL::to('js/jquery.js')}}></script>
	<script type="text/javascript" src={{URL::to('js/underscore.js')}}></script>
	<script type="text/javascript" src={{URL::to('js/backbone.js')}}></script>
	<link rel="stylesheet" type="text/css" href={{URL::to('css/topcoat-mobile-dark.css')}}>
</head>
<body>
	<div id="notes">
		<input type="text" class="topcoat-text-input--large" id="content_form" name="newnote" placeholder="New note here...">
		<div id="notes">
			<label class="topcoat-radio-button">
				<input type="radio" name="category" value="note" checked>
				<div class="topcoat-radio-button__checkmark"></div>
				Note
				<input type="radio" name="category" value="joke">
				<div class="topcoat-radio-button__checkmark"></div>
				Joke
				<input type="radio" name="category" value="task">
				<div class="topcoat-radio-button__checkmark"></div>
				Task
			</label>
		</div>
		<button class="topcoat-button" id="add-note">Save</button>

		<div class="topcoat-list">
			<h3 class="topcoat-list__header">Notes</h3>
			<ul class="topcoat-list__container">
				<li class="topcoat-list__item">
					Item
				</li>
			</ul>
		</div>

	</div>	

	<script type="text/javascript" src={{URL::to('js/app.js')}}></script>
</body>
</html>

