<?php

class NoteController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$notes = Note::all();
		return Response::json($notes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('note.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//return Response::json(Input::get('title')); 
		echo "Input::all():<br>".Response::json(Input::all());
		$note = new Note;
		$note->title = Input::get('title');
		$note->category = Input::get('category');
		$note->content = Input::get('content');
		$note->important = Input::get('important');
		$note->save();
		//return json_encode($note);
		echo "<br>NOTE:<br>";
		return Response::json($note);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$note = Note::find($id);
		return Response::json($note);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$note = Note::find($id);
		//var_export($_REQUEST); exit();
		//return Response::json($note);
		echo "FOUND:<br>".Response::json($note);
		echo "<br>Input::all():<br>".Response::json(Input::all());
		$note->title = Input::get('title');
		$note->category = Input::get('category');
		$note->content = Input::get('content');
		$note->important = Input::get('important');
		$note->save();
		//return json_encode($note);
		echo "<br>NOTE:<br>";
		return Response::json($note);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$note = Note::find($id);
		$note->delete();
		return Response::json('success');
	}

}